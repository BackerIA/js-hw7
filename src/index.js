// Опишіть своїми словами як працює метод forEach.
// Метод forEach застосовує функцію (певнії дії) до кожного елемента масиву.
// array.forEach(function(item, index, array) {
// дії, які виконуємо
// })

// Як очистити масив?
// arr.length = 0 або arr.splice(0, arr.length) або arr = [];

// Як можна перевірити, що та чи інша змінна є масивом?
// метод Array.isArray(array), поверне true якщо це массив

const arr = ["hello", "world", 23, "23", null];
const typeOfData = 'string';

function filterBy(array, typeOfData) {
    const result = [];
    array.forEach((item) => {
        if (typeof(item) !== typeOfData){
            if (!(item === null && typeOfData === 'null')){
                result.push(item);
            }
        }
    });
    return result;
}
console.log(filterBy(arr, typeOfData));
